<?php namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model
{
    protected $table = 'property';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id','image_path','property_name','default_pic','property_latitude','property_longitude','num_bedroom','num_bathroom','num_carparking','upper_price_value',
								'middle_price_value','bottom_price_value','property_type','available_for','short_description','landsize_approx','buildings_approx','property_desc','country',	
								'state','city','area','fullname','contact_number','company_name','company_logo'];
}
