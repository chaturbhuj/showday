-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 20, 2021 at 05:08 AM
-- Server version: 8.0.21
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `showday`
--

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS `property`;
CREATE TABLE IF NOT EXISTS `property` (
  `id` int NOT NULL AUTO_INCREMENT,
  `image_path` longtext NOT NULL,
  `property_name` varchar(50) NOT NULL,
  `default_pic` varchar(50) NOT NULL,
  `property_latitude` varchar(20) NOT NULL,
  `property_longitude` varchar(20) NOT NULL,
  `num_bedroom` int NOT NULL,
  `num_bathroom` int NOT NULL,
  `num_carparking` int NOT NULL,
  `upper_price_value` int NOT NULL,
  `middle_price_value` int NOT NULL,
  `bottom_price_value` int NOT NULL,
  `property_type` varchar(50) NOT NULL,
  `available_for` varchar(20) NOT NULL,
  `short_description` text NOT NULL,
  `landsize_approx` int NOT NULL,
  `buildings_approx` int NOT NULL,
  `property_desc` longtext NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `area` varchar(100) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `contact_number` int NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `company_logo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `image_path`, `property_name`, `default_pic`, `property_latitude`, `property_longitude`, `num_bedroom`, `num_bathroom`, `num_carparking`, `upper_price_value`, `middle_price_value`, `bottom_price_value`, `property_type`, `available_for`, `short_description`, `landsize_approx`, `buildings_approx`, `property_desc`, `country`, `state`, `city`, `area`, `fullname`, `contact_number`, `company_name`, `company_logo`) VALUES
(1, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_1.jpg', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(2, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_2.jpg', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(3, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_3.jfif', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(4, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_4.jpeg', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(5, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_5.jfif', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(6, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_6.jfif', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(7, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_7.jfif\r\n', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(8, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_8.jpg', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(9, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_1.jpg\r\n', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(10, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_8.jpg', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(11, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_6.jfif\r\n', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(12, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_6.jfif\r\n', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(13, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_6.jfif\r\n', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(14, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_6.jfif\r\n', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(15, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_6.jfif\r\n', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(16, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_6.jfif\r\n', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(17, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_8.jpg', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(18, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_8.jpg', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(19, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_8.jpg', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png'),
(20, '[\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_1.jpg\",\"property_8.jpg\",\"property_1.jpg\",\"property_1.jpg\",\"property_2.jpg\",\"property_8.jpg\"]', 'Spacious 2 bed 2 bath in boomed area', 'property_6.jfif\r\n', '12.42', '53.41', 2, 2, 1, 120, 110, 100, 'townhouse', 'sale', 'Spacious 2 bed 2 bath in boomed area', 10, 5, 'Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area.Spacious 2 bed 2 bath in boomed area', 'India', 'maharastra', 'mumbai', 'porch', 'Pradeep singh', 23232323, 'Show day', 'logo.png');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
